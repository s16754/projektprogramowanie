#include <iostream>
#include <conio.h>
using namespace std;

class Game{
private:
    char map[25][25];
    int snake_headX = 12;
    int snake_headY = 12;
    int snake_length = 3;
    char direction = 'u';

    int get_snake_headX() const{
        return snake_headX;
    }

    int get_snake_headY() const{
        return snake_headY;
    }


    char get_direction(){
        return direction;
    }

    void change_direction(char new_direction){
        direction = new_direction;
    }

    void move_snake(){
        map[get_snake_headY()][get_snake_headX()] = '#';
    }

    void move_snake_control(char coordinate, int input){
        if (coordinate == 'x'){
            move_snake();
            snake_headX += input;
            map[get_snake_headY()][get_snake_headX()] = '@';
        }
        else {
            move_snake();
            snake_headY += input;
            map[get_snake_headY()][get_snake_headX()] = '@';
        }
    }

    bool range_checker(char coordinate, int value){
        if (coordinate == 'x')
            return snake_headX+value <= 25 && snake_headX+value >= 0;
        else
            return snake_headY+value <= 25 && snake_headY+value >= 0;
    }

    void place_snake(){
        map[get_snake_headY()][get_snake_headX()] = '@';
    }

public:
    bool user_input(int input) {
        switch(input) {
            case 'w':    // UP
                move_snake_control('y',1);
                change_direction('u');
                return range_checker('y', 1);
            case 's':    // DOWN
                move_snake_control('y',-1);
                change_direction('d');
                return range_checker('y', -1);
            case 'a':    // LEFT
                move_snake_control('x',-1);
                change_direction('l');
                return range_checker('x', -1);
            case 'd':    // RIGHT
                move_snake_control('x',1);
                change_direction('r');
                return range_checker('x', 1);
            default:
                cout<<"Wrong Input"<<input<<"\n";
        }
        return true;
    }

    void printer() {
        vertical_border();
        for(int i = sizeof map[0]; i-->0; ){
            cout<<"*";
            for(auto& cell: map[i]){
                cout<<cell;
            }
            cout<<"*";
            cout<<"\n";
        }
        vertical_border();
    }

    void vertical_border(){
        for(int i=0; i<(sizeof map[0]+2); i++){
            cout<<"*";
        }
        cout<<"\n";
    }

    void generate_map() {
        for(auto& row: map){
            for(auto& cell: row){
                cell = ' ';
            }
        }
        place_snake();
    }
};

int main() {
    Game snake;
    bool game_is_working = true;

    snake.generate_map();;
    snake.printer();

    while(game_is_working) {
        game_is_working = snake.user_input(getch());
        system("cls");
        snake.printer();
    }

    getch();
    return 0;
}
/*Należy przygotować klasyczną grę Snake:

Plansza powinna mieć wymiar 25 na 25 komórek, a jej krawędzie powinny wyznaczać znaki gwiazdek (*)

Wąż powinien być reprezentowany przez połączone znaki hasztagu (#), a jego głowa powinna być oznaczona znakiem małpy (@)

Wąż zawsze zaczynać grę od środka planszy, w kierunku górnej krawędzi planszy, z długością 3 segmentów (głowa + 2 segmenty ogona)

Sterowanie wężem powinno następować klawiszami strzałek

Na jednym z segmentów planszy powinna pojawiać się porcja jedzenia.
Po każdorazowym spożyciu porcji przez węża w losowej komórce, która nie jest zajęta przez żaden z segmentów węża.

Po każdym zjedzeniu porcji pożywienia, wąż powinien się wydłużać o 1 segment.

Próba wyjścia węża poza planszę lub najechanie węża na swój ogon powinno kończyć grę z komunikatem wskazującym, ile punktów
udało się zdobyć graczowi. Jedna zjedzona porcja pożywienia = jeden zdobyty punkt.
*/